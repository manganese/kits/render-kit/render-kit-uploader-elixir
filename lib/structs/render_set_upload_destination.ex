defmodule Manganese.RenderKitUploader.Structs.RenderSetUploadDestination do
  @moduledoc """

  """

  alias Manganese.CoreKit
  alias Manganese.RenderKitUploader.Structs

  @typedoc """

  """
  @type t :: %Structs.RenderSetUploadDestination{
    rendered: CoreKit.Structs.Link.t,
    color: CoreKit.Structs.Link.t,
    normal: CoreKit.Structs.Link.t,
    depth: CoreKit.Structs.Link.t
  }
  @enforce_keys [
    :rendered,
    :color,
    :normal,
    :depth
  ]
  defstruct [
    :rendered,
    :color,
    :normal,
    :depth
  ]


  # Deserialization

  @doc """

  """
  @spec from_map(map) :: t
  def from_map(%{
    "rendered" => rendered,
    "color" => color,
    "normal" => normal,
    "depth" => depth
  }) do
    %Structs.RenderSetUploadDestination{
      rendered: CoreKit.Structs.Link.from_map(rendered),
      color: CoreKit.Structs.Link.from_map(color),
      normal: CoreKit.Structs.Link.from_map(normal),
      depth: CoreKit.Structs.Link.from_map(depth)
    }
  end


  # Serialization

  @doc """

  """
  @spec to_map(t) :: map
  def to_map(%Structs.RenderSetUploadDestination{
    rendered: rendered,
    color: color,
    normal: normal,
    depth: depth
  }) do
    %{
      "rendered" => CoreKit.Structs.Link.to_map(rendered),
      "color" => CoreKit.Structs.Link.to_map(color),
      "normal" => CoreKit.Structs.Link.to_map(normal),
      "depth" => CoreKit.Structs.Link.to_map(depth)
    }
  end
end