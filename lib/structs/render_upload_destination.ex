defmodule Manganese.RenderKitUploader.Structs.RenderUploadDestination do
  @moduledoc """

  """

  alias Manganese.CoreKit
  alias Manganese.RenderKitUploader.Structs

  @typedoc """

  """
  @type t :: %Structs.RenderUploadDestination{
    rendered: CoreKit.Structs.Link.t
  }
  @enforce_keys [
    :rendered
  ]
  defstruct [
    :rendered
  ]


  # Deserialization

  @doc """

  """
  @spec from_map(map) :: t
  def from_map(%{
    "rendered" => rendered
  }) do
    %Structs.RenderUploadDestination{
      rendered: CoreKit.Structs.Link.from_map(rendered)
    }
  end


  # Serialization

  @doc """

  """
  @spec to_map(t) :: map
  def to_map(%Structs.RenderUploadDestination{
    rendered: rendered
  }) do
    %{
      "rendered" => CoreKit.Structs.Link.to_map(rendered)
    }
  end
end