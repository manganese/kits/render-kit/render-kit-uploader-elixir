defmodule Manganese.RenderKitUploader.Structs.OrthographicProjectionSetUploadDestination do
  @moduledoc """

  """

  alias Manganese.CoreKit
  alias Manganese.RenderKitUploader.Structs

  @typedoc """

  """
  @type t :: %Structs.OrthographicProjectionSetUploadDestination{
    north: CoreKit.Structs.Link.t,
    east: CoreKit.Structs.Link.t,
    south: CoreKit.Structs.Link.t,
    west: CoreKit.Structs.Link.t
  }
  @enforce_keys [
    :north,
    :east,
    :south,
    :west
  ]
  defstruct [
    :north,
    :east,
    :south,
    :west
  ]


  # Deserialization

  @doc """

  """
  @spec from_map(map) :: t
  def from_map(%{
    "north" => north,
    "east" => east,
    "south" => south,
    "west" => west
  }) do
    %Structs.OrthographicProjectionSetUploadDestination{
      north: CoreKit.Structs.Link.from_map(north),
      east: CoreKit.Structs.Link.from_map(east),
      south: CoreKit.Structs.Link.from_map(south),
      west: CoreKit.Structs.Link.from_map(west)
    }
  end


  # Serialization

  @doc """

  """
  @spec to_map(t) :: map
  def to_map(%Structs.OrthographicProjectionSetUploadDestination{
    north: north,
    east: east,
    south: south,
    west: west
  }) do
    %{
      "north" => CoreKit.Structs.Link.to_map(north),
      "east" => CoreKit.Structs.Link.to_map(east),
      "south" => CoreKit.Structs.Link.to_map(south),
      "west" => CoreKit.Structs.Link.to_map(west)
    }
  end
end