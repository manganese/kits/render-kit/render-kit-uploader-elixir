defmodule Manganese.RenderKitUploader.MixProject do
  use Mix.Project

  def project do
    [
      app: :manganese_render_kit_uploader,
      version: "0.2.3",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [ :logger ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # ExDoc
      { :ex_doc, "~> 0.19", only: :dev, runtime: false },

      # Manganese CoreKit
      { :manganese_core_kit, "~> 0.1" }
    ]
  end

  defp package do
    [
      name: "manganese_render_kit_uploader",
      description: "Library for using RenderKit uploader data structures in Elixir",
      licenses: [ "AGPL v3.0" ],
      links: %{
        "GitLab" => "https://gitlab.com/manganese/kits/render-kit/render-kit-uploader-elixir"
      },
      maintainers: [
        "Samuel Goodell"
      ]
    ]
  end
end
