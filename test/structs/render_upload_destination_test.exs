defmodule Manganese.RenderKitUploaderTest.Structs.RenderUploadDestinationTest do
  use ExUnit.Case

  alias Manganese.CoreKit
  alias Manganese.RenderKitUploader.Structs

  doctest Manganese.RenderKitUploader.Structs.RenderUploadDestination

  # Deserialization
  describe "`Manganese.RenderKitUploader.Structs.RenderUploadDestination.from_map/1`" do
    @tag render__upload_destination: true
    test "it deserializes an render upload destination from a map" do
      rendered_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/render/rendered.png"

      map = %{
        "rendered" => CoreKit.Structs.Link.to_map(rendered_link)
      }
      render__upload_destination = Structs.RenderUploadDestination.from_map map

      assert render__upload_destination.rendered == rendered_link
    end
  end

  # Serialization
  describe "`Manganese.RenderKitUploader.Structs.RenderUploadDestination.to_map/1`" do
    @tag render__upload_destination: true
    test "it serializes an render upload destination to a map" do
      rendered_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/render/rendered.png"

      render__upload_destination = %Structs.RenderUploadDestination{
        rendered: rendered_link
      }
      map = Structs.RenderUploadDestination.to_map render__upload_destination

      assert CoreKit.Structs.Link.from_map(map["rendered"]) == rendered_link
    end
  end
end
