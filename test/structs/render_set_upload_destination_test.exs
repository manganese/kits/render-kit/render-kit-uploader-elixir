defmodule Manganese.RenderKitUploaderTest.Structs.RenderSetUploadDestinationTest do
  use ExUnit.Case

  alias Manganese.CoreKit
  alias Manganese.RenderKitUploader.Structs

  doctest Manganese.RenderKitUploader.Structs.RenderSetUploadDestination

  # Deserialization
  describe "`Manganese.RenderKitUploader.Structs.RenderSetUploadDestination.from_map/1`" do
    @tag render_set_upload_destination: true
    test "it deserializes an render set upload destination from a map" do
      rendered_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/render-set/rendered.png"
      color_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/render-set/color.png"
      normal_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/render-set/normal.png"
      depth_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/render-set/depth.png"

      map = %{
        "rendered" => CoreKit.Structs.Link.to_map(rendered_link),
        "color" => CoreKit.Structs.Link.to_map(color_link),
        "normal" => CoreKit.Structs.Link.to_map(normal_link),
        "depth" => CoreKit.Structs.Link.to_map(depth_link)
      }
      render_set_upload_destination = Structs.RenderSetUploadDestination.from_map map

      assert render_set_upload_destination.rendered == rendered_link
      assert render_set_upload_destination.color == color_link
      assert render_set_upload_destination.normal == normal_link
      assert render_set_upload_destination.depth == depth_link
    end
  end

  # Serialization
  describe "`Manganese.RenderKitUploader.Structs.RenderSetUploadDestination.to_map/1`" do
    @tag render_set_upload_destination: true
    test "it serializes an render set upload destination to a map" do
      rendered_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/render-set/rendered.png"
      color_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/render-set/color.png"
      normal_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/render-set/normal.png"
      depth_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/render-set/depth.png"

      render_set_upload_destination = %Structs.RenderSetUploadDestination{
        rendered: rendered_link,
        color: color_link,
        normal: normal_link,
        depth: depth_link
      }
      map = Structs.RenderSetUploadDestination.to_map render_set_upload_destination

      assert CoreKit.Structs.Link.from_map(map["rendered"]) == rendered_link
      assert CoreKit.Structs.Link.from_map(map["color"]) == color_link
      assert CoreKit.Structs.Link.from_map(map["normal"]) == normal_link
      assert CoreKit.Structs.Link.from_map(map["depth"]) == depth_link
    end
  end
end
